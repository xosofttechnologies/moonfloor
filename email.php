<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <style type="text/css">
        /* FONTS */
        @media screen {
            @font-face {
                font-family: \'Lato\';
                font-style: normal;
                font-weight: 400;
                src: local(\'Lato Regular\'), local(\'Lato-Regular\'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format(\'woff\');
            }

            @font-face {
                font-family: \'Lato\';
                font-style: normal;
                font-weight: 700;
                src: local(\'Lato Bold\'), local(\'Lato-Bold\'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format(\'woff\');
            }

            @font-face {
                font-family: \'Lato\';
                font-style: italic;
                font-weight: 400;
                src: local(\'Lato Italic\'), local(\'Lato-Italic\'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format(\'woff\');
            }

            @font-face {
                font-family: \'Lato\';
                font-style: italic;
                font-weight: 700;
                src: local(\'Lato Bold Italic\'), local(\'Lato-BoldItalic\'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format(\'woff\');
            }
        }

        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* MOBILE STYLES */
        @media screen and (max-width: 600px) {
            h1 {
                font-size: 32px !important;
                line-height: 32px !important;
            }
        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style>
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->


<table border="0" cellpadding="0" cellspacing="0"align="center">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#deb887" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%;">
                <tr>
                    <td align="center" valign="top" style="padding: 15px 10px 10px 10px;">
                        <a href="#" target="_blank">
                            <img alt="Logo" src="http://www.moonfloors.com/mf_logo.png"

                                 style="display: block; width: 100px; max-width: 100px; min-width: 24px; font-family: \'Lato\', Helvetica, Arial, sans-serif; color: #ffffff; font-size: 18px;"
                                 border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- HERO -->
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->

            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- COPY BLOCK -->
    <tr>
        <td bgcolor="#f4f4f4" align="center" >
            <!--[if (gte mso 9)|(IE)]>

            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%;">
                <!-- COPY -->
                <tr>
                    <td bgcolor="#ffffff" align="left"
                        style='padding: 25px 30px 0px 49px; color: black; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 25px;'>
                        <h5 style="margin: 0; font-size:14px;margin-bottom:10px;">Name: <span>{{name}}</span></h5>
                        <h5 style="margin: 0; font-size:14px;margin-bottom:10px;">Email: <span>{{email}}</span></h5>
                        <h5 style="margin: 0; font-size:14px;margin-bottom:10px;">Phone: <span>{{phone}}</span></h5>
                        <div> <h5 style="margin: 0; font-size:14px;margin-bottom:10px; float: left;">Message:</h5></div>
                        <div style=" width: 600px;margin-left: 70px">{{mesg}}</div>



                    </td>
                </tr>

            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!-- SUPPORT CALLOUT -->
    <!-- FOOTER -->
<tr>
    <td bgcolor="#f4f4f4" align="center">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
            <tr>
                <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%;">
            <!-- COPY -->
            <tr>
                <td bgcolor="#ffffff" align="left"
                    style='padding: 0px 30px 20px 49px; border-radius: 0px 0px 4px 4px; color: #111111; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
                    <p style="margin-top:10px;"><strong>Best Regards, <br/></strong><i
                                style="font-size: 16px;">Moonfloors Support Team</i></p>
                </td>
            </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
    </td>
</tr>
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <!-- COPY -->
                <tr>
                    <td bgcolor="#f4f4f4" align="left"
                        style="padding: 30px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #111111; font-family: \'Lato\', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: 400; line-height: 17px;text-align: center;">
                        <p style="margin: 0;">You\'re receiving this email notification because you requested on <a
                                href="https://www.moonfloors.com/">moonfloors.com</a>. This message was sent by
                            moonfloor Inc, Do not reply to this email, it has been automatically generated.</p>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
</table>

</body>
</html>