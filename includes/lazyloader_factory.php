<ul class="list-unstyled row portfolio-box">
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="img/factory%20tour/1.jpg">
            <img class="full-width img-responsive" src="img/factory%20tour/1.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery" href="img/factory%20tour/2.jpg">
            <img class="full-width img-responsive" src="img/factory%20tour/2.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4">
        <a class="thumbnail fancybox" data-rel="gallery"  href="img/factory%20tour/3.jpg">
            <img class="full-width img-responsive" src="img/factory%20tour/3.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>
    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="img/factory%20tour/4.jpg">
            <img class="full-width img-responsive" src="img/factory%20tour/4.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="img/factory%20tour/5.jpg">
            <img class="full-width img-responsive" src="img/factory%20tour/5.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4">
        <a class="thumbnail fancybox" data-rel="gallery"  href="img/factory%20tour/6.jpg">
            <img class="full-width img-responsive" src="img/factory%20tour/6.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>
    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="img/factory%20tour/7.jpg">
            <img class="full-width img-responsive" src="img/factory%20tour/7.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="img/factory%20tour/8.jpg">
            <img class="full-width img-responsive" src="img/factory%20tour/8.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4">
        <a class="thumbnail fancybox" data-rel="gallery"  href="img/factory%20tour/9.jpg">
            <img class="full-width img-responsive" src="img/factory%20tour/9.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>
    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="img/factory%20tour/10.jpg">
            <img class="full-width img-responsive" src="img/factory%20tour/10.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4 md-margin-bottom-50">
        <a class="thumbnail fancybox" data-rel="gallery"  href="img/factory%20tour/11.jpg">
            <img class="full-width img-responsive" src="img/factory%20tour/11.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>

    </li>
    <li class="col-sm-4">
        <a class="thumbnail fancybox" data-rel="gallery"  href="img/factory%20tour/12.jpg">
            <img class="full-width img-responsive" src="img/factory%20tour/12.jpg" alt="">
            <span class="portfolio-box-in">
                            <i class="rounded-x icon-magnifier-add"></i>
                        </span>
        </a>
    </li>
</ul>