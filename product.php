<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/html"> <!--<![endif]-->
<head>
    <title>Products</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <!-- <link rel="shortcut icon" href="favicon.ico">-->

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css'
          href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-v2.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/fancybox/source/jquery.fancybox.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">
    <style>

        h1, h3 {
            color: black;
        }
    </style>
</head>

<body class="header-fixed">

<div class="wrapper">
    <!--=== Header v2 ===-->
  <?php include('header.php'); ?>
    <!--=== End Header v2 ===-->
    <!-- Image Gradient -->
    <div class="interactive-slider-v2">
        <div class="container">
            <h1>Welcome to MOON MARBLES</h1>
            <p>Best Quality all over the Pakistan.</p>
        </div>
    </div>

    <?php
    if ($_GET['var'] == 'golden_camel'):
        ?>
        <div class="container content-sm">

            <div class="row  margin-bottom-30">
                <span><img class="img-responsive" src="img/products/1.jpg" width="100%" height="100%" alt=""></span>
            </div>
            <h1 class="w-blog-post-title entry-title">Golden Camel Marble</h1><br>
            <strong></strong>
            <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
            <br>
            <h3>Golden camel marble the perfect piece that always fits in. Classic and graceful, golden camel marble tiles and slabs carry mellowness and coziness in equal parts and all in a very natural and melodious manner. The most classic, magnificent and luxurious natural stone marble perfect for any residential and commercial area. Golden camel has a realistic natural slate visual and elegant texture. Its natural, beautiful and rich color add a unique statement of elegance.</h3>
            <h3>Golden Camel stone adds an eye-catching attention to your home and living room. Bring the look of rich in any space is not only durable enough for bathrooms, kitchens and living areas, golden camel can also be used in exterior and commercial environments. Golden Camel marble is available in different size cut-to-size tiles and slabs 20mm and 30mm Thickness and above in competitive market rates.</h3>
            <strong></strong>
            <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
            <br><strong><h4>Type of material:</strong> Marble</h4><br>
            <strong></strong>
            <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
            <ul>
                <li><h4>Good service and competitive price</h4></li>
                <li><h4>Professional stone manufacturer</h4></li>
            </ul>
        </div>
    <?php
    elseif ($_GET['var'] == 'Lakrol Fancy'):
        ?>
        <div class="container content-sm">

            <div class="row  margin-bottom-30">
                <span><img class="img-responsive" src="img/products/2.jpg" width="100%" height="100%" alt=""></span>
            </div>
            <h1 class="w-blog-post-title entry-title">Lakrol Fancy Marble</h1><br>
            <strong></strong>
            <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
            <br>
            <h3>Lakrol Fancy Marble is the wavy type of marble which can be used in bathroom, kitchen and flooring. It looks so attractive which is the best thing in this natural marble stone. Lakrol Fancy Marble is also known as Desert wave marble. Lakrol Fancy Marble is now the most valuable marble in the market because of its beauty and gorgeous wavy texture.</h3>
           <h3>The high quality texture and elegant design are the qualities of the Lakrol Fancy Marble. Available in both polished finishes and honed, this elegant natural marble stone is the ultimate choice to make stunning marble kitchen, marble counter tops, marble tile floors, and backsplashes. The most luxurious marble available in both slabs and tiles, this natural stone is the best choice for interior projects entryways, flooring and any business area. We recommend our client to make your choice Lakrol Fancy Marble.</h3>
            <strong></strong>
            <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
            <br><strong><h4>Type of material:</strong> Marble</h4><br>
            <strong></strong>
            <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
            <ul>
                <li><h4>Good service and competitive price</h4></li>
                <li><h4>Professional stone manufacturer</h4></li>
            </ul>
        </div>
    <?php
    elseif ($_GET['var'] == 'Royal Fancy'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/3.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Royal Fancy Marble</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
        <h3>Royal Fancy Marble is well known for its beautiful texture and its decent color. This fancy marble has various textures and shades like mildish light fancy, classy dark fancy, aggressive desert storm fancy cross and cut royal fancy marble. This is really best for flooring, bathroom, kitchen, countertops, vanity tops, window sills, wall tiles, project tiles and stair etc.</h3>
        <h3>This natural marble also occurs veins and shades by nature. The veining of the marble helps out to the appearance and longevity of the stone, masking the floor inevitable wear and tear over the years. The advantages of Royal Fancy Marble could be very clean to work with. it’s far a softer stone that can be milled, machined and tumbled, this means that it is able to have a couple of makes use of.</h3>

        <strong></strong>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Marble</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Pakistani Cicilia'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/4.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Pakistani Cicilia Marble</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
        <h3>Pakistani Cicilia marble is well known because of its beautiful design and elegant texture. We as a reputed firm and our company considered as a leading company that manufacture, supply and trade high quality and a large range of Pakistani Cicilia marble. We provide first-class quality material all over the world. Our marble is elegantly textured and beautifully polished. Top of that we offered Pakistani Cicilia marble at competitive market rates.</h3><strong></strong>
        <h3>Cicilia marble also known as many different names such as Perlato cicilia marble, Parlato stone, Perlato granite, Perlato beige marble, Cicilia granite, Cicilia royal stone, Perlato floor tile, Perlato cicilia marble pakistan, Pakistani Perlato cicilia marble,Perlato sicilia marble granite, Cicilia marble slab, Perlato marble slab and tiles, Italian Perlato marble, Italian cicilia marble, Indian cicilia marble, Europian perlato, Perlato classico stone, China Perlato stone, China cicilia marble, china sicilia marble.</h3>
       <h3>Many landlords choose this stone to install in their kitchens, where its numerous tones, textures, and grains bring a completely unique, modern-day aesthetic with the undying appeal. Pakistani Cicilia marble with a huge, abstract pattern makes for a clean complement to natural wood and shiny copper pendants.</h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Marble</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Fossil Brown'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/5.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Fossil Brown Marble</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
       <h3>Fossil brown marble has its own attractive and pleasing texture. Fossil brown is also known as Bruno Etrusco, Brown Fossil, Fossil Marrone, Marron Etrusco, Brown Fossil Limestone, Fossil Marmor braun, Jurassic sea marble, Cānghǎi zhū luó (in China stone market). As like its unique names Fossil Brown Marble has its unique specification. In the interiors of residential and industrial buildings, it is mostly used for flooring, countertops and other wall applications.</h3>
       <h3>
           Fossil Brown marble is the most versatile type of natural stone for wall tile, floor tile, project tile, stair, windows sills, countertop, vanity top etc. Fossil Brown Marble is splendid and, appears elegantly in spite of everything finishing has been accomplished, Marble can be used as wall cladding, bar top, fireplace surround, sinks base, light duty home floors, and tables. With a unique blend of brown and grey shades and dark brown veining, this elegant marble will stand the test of time.
       </h3> <strong></strong>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Marble</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Sahara Beige'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/6.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Sahara Beige Marble</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
        <h3>
            Sahara Beige Marble is one of the most popular varieties of Pakistan marble. This marble comes in Pink and beige, chocolate, red, brown green and gray shades. Low water absorption, high hardness, resistance to heat and weather corrosion, uniform color and luster make it a popular variety. The most gorgeous beige marble that has been tumbled to offer a gentle aspect.
        </h3>
       <h3>Sahara Beige is expensive and evidently precise, Marble is a sought-after stone that provides glamour to any living space. Famous in halls and Bathrooms, its dense structure makes it ideal for frequently-used areas.</h3>
        <strong></strong>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Marble</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Nova Beige'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/7.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Nova Beige Marble</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
        <h3>
            As a kind of beige marble from Pakistan, Nova Beige Marble is commonly used in Building stone, fireplaces, stairs, countertops, sinks, monuments, pool coping, pavers, sills, ornamental stone, mosaic, fountain, sculpture and so on. Nova Beige Marble can also be called Nova Beige, Nova Bej, Mersin Beige and so on. It can be turned into different surface finishing: Polished, Sawn Cut, Sanded, Rockfaced, Sandblasted, Tumbled, etc.
        </h3>
       <h3>

           The Nova Beige is one of the most relevant beige marbles from Pakistan, with high demand and supply. The Nova Beige has a large market both in Turkey and abroad. It is extracted in different locations by a large number of quarries, with diverse dimensions.
       </h3>
       <h3>
           This marble is considered by many as the most relevant beige marble from Pakistan and a potential candidate to replace the Crema Marfil from Spain as the most relevant beige marble on the international market.
       </h3>
        <strong></strong>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Marble</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Bubble Limestone'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/8.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Bubble Limestone</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
        <h3>
            Bubble Limestone is a natural stone, just like marble, granite, and limestone—in fact, bubble marble is a type of limestone, which can be use vanity top, Wall tile, window sills, floor tile, project tile, stair, etc counter top, fountain, column, baluster, handrail,etc statue, curb stone, etc. The main advantage of Bubble limestone is its beauty.
        </h3>
       <h3>
           Hardness (Mosh):  3
           Weight per unit of volume (gr/cm3) 2,695
           Degree of porosity % 0,195
           Humidity %  0,109
           Water absorption at boiling water %  99,75
           Strength to impact (Kgf/cm2)  3,44
           Compressive strength after frost (Kgf/cm2)  74,1
           Strength to bending (Kgf/cm2) 141,28
           Uniaxial compressive strength (kgf/cm2) 933,74
           Kohesyon (kgf/cm2) 202,24
           Internal friction angle (o) 49,73
           Elastisite module (GPa) 35,37
           Poisson  0,316
           Point load strength (kgf/cm2) 57,59
           Fullness rate (%) 99,8
       </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Stone </h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Flower Beige'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/9.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Flower Beige Marble</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
       <h3>
           Flower Beige Marble is a kind of beige marble quarried in Turkey. This stone is especially good for Exterior – Interior wall and floor applications, monuments, countertops, mosaic, fountains, pool and wall capping, stairs, window sills, etc and other design projects. It also called Light Flower Beige Marble, Flowered Beige Marble, Amasya Beige Marble. Flower Beige Marble can be processed into Polished, Sawn Cut, Sanded, Rockfaced, Sandblasted, Tumbled and so on.
       </h3>
        <h3>
            Hardness (Mosh):  3
            Weight per unit of volume (gr/cm3) 2,695
            Degree of porosity % 0,195
            Humidity %  0,109
            Water absorption at boiling water %  99,75
            Strength to impact (Kgf/cm2)  3,44
            Compressive strength after frost (Kgf/cm2)  74,1
            Strength to bending (Kgf/cm2) 141,28
            Uniaxial compressive strength (kgf/cm2) 933,74
            Kohesyon (kgf/cm2) 202,24
            Internal friction angle (o) 49,73
            Elastisite module (GPa) 35,37
            Poisson  0,316
            Point load strength (kgf/cm2) 57,59
            Fullness rate (%) 99,8
        </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Marble</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Travertine Marble'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/10.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Travertine Marble</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
        <h3>
            Travertine marble is a natural stone, just like marble, granite, and limestone—in fact, travertine is a type of limestone. However, they are not the same. Travertine has some telltale characteristics that separate it from regular limestone. One key trait is the holes found within the stone. These are caused by carbon dioxide evasion. Travertine can range in color from ivory and beige to walnut and gold. The color is determined by the amount of iron or other organic impurities found in the particular slab of travertine.
        </h3>
        <h3>
            The main advantage of travertine is its beauty. The stone comes in many beautiful colors — including ivory, beige, walnut, and gold — that can add to the beauty of any room. Travertine stone flooring that has been polished and sealed is easy to clean and is considered to be both hygienic and environmentally friendly. This type of prepared travertine can help contribute to the air quality of your home as it does not absorb odors, chemicals, or gases. Dirt stays on the surface of the sealed stone, rather than being ground in, and can easily be removed.
        </h3>
        <h3>
            If you want a unique and beautiful flooring option for your home, travertine flooring is an excellent choice. It is environmentally friendly, decorative, and a quality investment.
        </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Marble</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Butak Onyx'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/11.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Butak Onyx</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
       <h3>
           MOON Marble produces high-quality Butak Onyx tiles. The working of the Butak Green Onyx tiles requires a special attention during the selection phase, that is the quality check of the materials. The tiles obtained from the best blocks, are finally checked and selected one by one, so as to be able to assure the final Customers a high quality and excellent selection.
       </h3>
        <h3>
            Extracted from its origins in the vicinity of the village of Butak, this transparent material follows a soft pattern which outshines the simplicity of this piece. The light celestial pattern adds a hint of sophistication to the material.
        </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Onyx</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Yellow Onyx'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/12.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Yellow Onyx</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
        <h3>
            Pakistan yellow onyx is popular yellow onyx from Pakistan onyx quarry, Pakistani onyx is famous in the world. Yellow onyx products include golden onyx tile for interior wall, golden onyx slabs, golden onyx countertop, gold onyx vanity tops and tub surrounds, golden onyx table top, gold onyx sink and basin, carvings, tiles and Slabs etc.
        </h3>
        <h3>
            Yellow Onyx is a kind of Onyx Tiles & Slabs of products, the company mainly export marble, granite and Artificial Stones mainly raw materials and finished goods, common products are Slabs, Tiles, Countertops, Stone Basins, Sandstone, Limestone, Tombstone, Kerbstones Wall Cladding, Carvings, Fireplace, Pebble Stones, among which Yellow Onyx processed stone products are exported to UAE, Europe, America, Russia, Saudia, Qatar, Korea, China, Lebanon, Italy, Bahrain and other countries.
        </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Onyx</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Coffee Brown'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/13.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Coffee Brown Marble</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
        <h3>
            After understanding the needs and expectations of the growing industry, we are presenting the Coffee Brown Marble. We manufacture and export the marbles in different shades and sizes. Available at the market leading rates, these marbles get duly tested by our quality auditors on various parameters. In addition, we deliver the Coffee Brown Marble to the clients according to their specified colors and designs.
        </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Marble</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'White Limestone'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/14.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">White Limestone</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
       <h3>
           White Limestone is especially good for Exterior – Interior wall and floor applications, mosaic, fountains, monuments, counter tops pool, stairs, window sills and wall capping etc. This limestone is popular for its natural and decent look. White limestone mostly used in hotels, restaurants and garden areas. This limestone looks simple and homely but it is more beautiful than other natural stones.
       </h3>
        <h3>
            White Limestone have been loved for centuries and recently becoming the hottest seller for Kitchen countertops, marble Bathroom countertops, Marble Feature Walls, Marble fireplace, marble tiles for bathrooms, floors etc. This natural limestone is one of the most luxurious limestone to add a sensational and remarkable look to your home and business area. White limestone marble is slightly penetrable and more velvety than marble. We have high quality white limestone tiles, slabs and blocks in a competitive market rates.
        </h3>

        <h1 style="font-size:40px"><u>Description:</u></h1></strong><br>
        <h3>
            1. Both Face Natural Or Top Face Natural Backside Calibrated
            2. Color: White
            3. All standard dimensions are available. Can be made according to customer’s requirements
            4. Regular reference size:
            –30x60x2cm
            –60x60x20cm
            –30xfree 2cm
            –100x100x3cm
            5. Regular reference thickness: 1.0/1.2 /1.8/2.0/3.0 (cm) etc.
            6. Volume density:3.3g/cm3
            7. Water absorption:0.09
        </h3>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Black and Gold'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/15.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Black and Gold Marble</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
      <h3>
          Black and gold marble (Michelangelo) is a very unique and famous Pakistani marble, it is famous because of its black color on which there are white & golden stripes. It is also called and knows as Black & Gold. It is found from the mountains of Balochistan province which is in the southwest part of Pakistan and this province is rich with other minerals too such as onyx, granite etc.
      </h3>
        <h3>
            Black and gold (Michelangelo) is an marble of which very few people know its proper origin and most people think that it is an Italian or Chinese stone as they buy it from Italy or China  , the reason  behind it is that  factory owners of Italy & China come to Pakistan and buy it in the form of mono lama blocks and then process them into tiles and slabs and re-export to other countries that is main reason why most of the buyer think that this stone is not of Pakistani origin but the truth is that it is from Pakistan and it is also processed in Pakistan and the finished tiles and slabs are of same quality to Italy & China and the price is almost 50% less than these countries but the quality is same.
        </h3>

        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Marble</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Light Green'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/16.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Light Green Onyx</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
     <h3>
         Pakistani Light Green Onyx is really attractive due to its light shades as well as stunning and a modern look. Because of its specific design, it offers striking and dramatic appeal, it is most demanding worldwide. Pakistan is considered a very lucky, wealthy and blessed because it has a huge amount of world’s best quality Onyx resources.
     </h3>

        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Onyx</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Afghan Green'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/17.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Afghan Green Onyx</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
      <h3>
          Afghan Green Onyx Slabs for export sold per square foot or meter. These slabs measure: Length: 4 to 6 feet OR 48 to 72 inches OR 120 to 180 cm Width: 3 to 5 feet OR 36 to 60 inches OR 90 to 150 cm and Onyx Slabs Thickness: 2 and 3 cm OR 20 and 30 mm, based on the characteristics of the materials. They are crack-free, therefore usable up to 90/95%, and suitable for every cut-to-size work, like tiles, countertops for shops, hotels, restaurants, bathrooms, stairs steps, Coffee shops, table tops, vanity tops etc
      </h3>
        <h3>
            Afghan Green Onyx Slabs are suggested to produce vanity for bathrooms, tiles, skirting, stairs and any kind of cut-to-size work of reduced dimension. They are suitable for markets where the labor cost is lower than in other countries, or where the work required to finish the product is not cost effective so that the cost of the finished work remains more competitive.
        </h3>
        <h3>
            Each stone, and therefore each floor, is unique in character, and variation must be expected. All slabs sizes and thickness are an approximation only and all Onyx slabs will vary in color, tone, marking and texture from those shown in the website/brochure or from samples received.
        </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Onyx</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Multi Red'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/18.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Multi Red Onyx</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
      <h3>
          A great alternative to granite, marble, and other more popular stone surfaces, onyx provides a sleek and modern look to any workspace. Onyx is formed in caves as stalactites and stalagmites drip to form this precious stone. Onyx Countertops are a rarity as they bring a unique appearance with a strikingly dramatic appeal. Not often placed in a horizontal orientation such as a countertop, onyx countertops are a unique and premium surface. With its exclusive style, onyx generally comes in swirling and pastel color patterns. We offer over 9 onyx color choices of this exceptional stone, in-stock and ready to be delivered for your unique project surfaces.
      </h3>
        <h3>
            Onyx Stone is a highly decorative material formed in banded layers of sediment near cold springs and it has been used as a gemstone and as a highly decorative surfacing material for centuries. Many varieties of onyx include semi-translucent veining that may be backlit, creating a dramatic effect. Our selection ranges from pearlescent white stones to deep red, and other vibrant colors. Onyx is best suited for interior walls, vertical surfaces, and vanity countertops or as a decorative trim.
        </h3>
        <h3>
            Typically more fragile, but often more beautiful than other natural stone surfacing options, when used properly onyx can add lots of impact to your designs. Our selection features many rare vein cut options showing a cross section of the natural banding in the layers of sediment. We also feature some premium cross cut options which generally allow for taller slabs and bigger patterns than the more linear vein cut options.
        </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Onyx</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Multi Green'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/19.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Multi Green Onyx</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
        <h3>
            Multi Green Onyx is a beautiful cryptocrystalline form of natural stone but it’s calcite like marble, it is denser, translucent and more brittle. It is extracted from Pakistan’s natural reserves existing in Baluchistan province. Pakistan has specific variations of Onyx, most common favorite is green among others.
        </h3>
        <h3>

            We fulfill the orders for multi green onyx in form of steps, slabs, tiles, raw blocks, bullnose, steps, and riser matching with the orders measurements and weight.

            We encourage importers to browse our website and talk to us via email sales@smbmarble.com or phone at 00923218888887.

            Whereas existing images on our website portray a real picture of the color and texture of the Multi Green Onyx material emphasize that examining products physically provide the real feel of color and texture of materials.
        </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Onyx</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'White Onyx'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/20.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">White Onyx</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
       <h3>
           MOON Marble completes the various orders most likely of raw material in form of white onyx blocks, finished and polished white onyx slabs and tiles. Because of its own beauty and attractiveness, the very specific audience get interested in white onyx. White onyx is a semi-precious stone as well as being translucent it brings special effects when backlit is transferred from the Onyx.
       </h3>
        <h3>
            MOON Marble believes in trustworthy relationships with the customer because this bonding helps in making successful business deals and proves profitable among both parties. Standing out of the plenty of Competition demands accuracy, reliability as well as timely production with exact specifications and delivery according to promises. Our fair dealings with our respected clients made us capable in white onyx importer’s eye to use our supplied material for some of the prestigious building projects in the Gulf region.
        </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Onyx</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Indus Gold'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/21.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Indus Gold Marble</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
        <h3>
            Indus Gold marble are available in varied golden colors, sizes and at affordable cheap price. It adds a timeless charm to the modern day building. With its lush dark golden color, it radiates a pleasing and soothing appeal, thus preserving its inherent beauty and vitality. Golden color elevate the mood of the people so they should be used in the house so that these slabs create a happy environment in the house always.
        </h3>
        <h3>
            Indus Gold marble tiles and slabs are the best for the ones’ who wish to have an elegant yet different looking home. They are very much in demand nowadays as people want to try something new for their home. These slabs should be definitely used in order to have a beautiful house which will be different from others in all possible manners  SMB Marble offers you a variety of Indus gold sandstone gangsaw slabs in various finishes at an affordable price so that ever one can make their home heaven.
        </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Marble</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Dark Green'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/22.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Dark Green Onyx</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
       <h3>
           MOON Marble owns quarries of Dark Green Onyx and other natural stones, therefore, has the ability to deliver the required quality of Dark Green Onyx. As having experience of more than two decades, we have a specialty in producing quality Onyx according to the requirement. Our production quality improves our valued customers’ satisfaction level. Pakistan is the only country among the few others who have the availability of onyx under its natural reserves.
       </h3>
       <h3>
           Usually, every landlord has the tremendous desire of getting commercial and residential buildings constructed in an artistic and wonderful way and that makes them satisfied. Pakistan is blessed by an abundance of natural minerals reserves including resources of green onyx. Light transmitting ability of Onyx really produces a striking effect. The luxurious and glamorous look of Onyx’ shiny surface is the cause of attraction for construction stone buyers.
       </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Onyx</h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    elseif ($_GET['var'] == 'Yellow Sandstone'):
    ?>
    <div class="container content-sm">

        <div class="row  margin-bottom-30">
            <span><img class="img-responsive" src="img/products/23.jpg" width="100%" height="100%" alt=""></span>
        </div>
        <h1 class="w-blog-post-title entry-title">Yellow Sandstone</h1><br>
        <strong></strong>
        <h1 style="font-size:50px"><u>Product Description</u></h1></strong>
        <br>
      <h3>
          Yellow Sandstone is a beautiful natural Sandstone exhibiting a mixture of yellow, buff, and golden colors. With its natural tones and shades, Yellow Sandstone paving blends in with almost any surrounding. Yellow is especially useful in exterior claddings in sea shore buildings due to acid and thermal resistant properties. As such the effect of saline winds is negligible on Yellow Sandstone. The Golden Yellow hue in this sandstone provides excellent opportunities for this sandstone for wide spread applications. The sunflower Yellow color gives it an inviting look and thus glowing any surrounding it is used in.
      </h3>
        <h3>
            Yellow Sandstone is something that will certainly get people’s attention, it is also a great choice for use around swimming pools as its smooth surface yet its grip make it ideal for bare feet and can help greatly against poolside slips. Yellow Sandstone is available in various formats as blocks, slabs, tiles and cobbles in different surface finishes, and in different sizes with thickness of 15 – 50mm and 18 – 22 mm. Yellow Sandstone accepts various value added finishes such as natural cleft, flamed, polished, honed, bush hammered, sawn, acid washed, antique finish and sand blasted to suit different applications.
        </h3>
        <h1 style="font-size:40px"><u>Product Details:</u></h1></strong>
        <br><strong><h4>Type of material:</strong> Stone </h4><br>
        <strong></strong>
        <h1 style="font-size:40px"><u>Specification:</u></h1></strong><br>
        <ul>
            <li><h4>Good service and competitive price</h4></li>
            <li><h4>Professional stone manufacturer</h4></li>
        </ul>
    </div>
    <?php
    endif;
    ?>

    <?php include ('footer.php'); ?>
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->


<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<script type="text/javascript" src="assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/plugins/fancy-box.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        App.init();
        FancyBox.initFancybox();
    });
</script>
<!--[if lt IE 9]>
<script src="assets/plugins/respond.js"></script>
<script src="assets/plugins/html5shiv.js"></script>
<script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>