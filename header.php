<?php $activePage = basename($_SERVER['PHP_SELF'], ".php"); ?>
<div class="header-v2 <?= ($activePage == 'catalogue' ||$activePage == 'factorytour' ) ? '' : 'header-sticky'; ?> ">
    <div class="container container-space">
        <!-- Topbar v2 -->

        <!-- End Topbar v2 -->
    </div>

    <!-- Navbar -->
    <div class="navbar navbar-default mega-menu" role="navigation">
        <div class="container container-space">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-responsive-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="fa fa-bars"></span>
                </button>
                <a class="navbar-brand brand-style" href="/">
                    <img id="logo-header" src="mf_logo.png" width="85" height="32" alt="Logo">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav">
                    <!-- Home -->
                    <li>
                        <a href="/">
                            Home
                        </a>

                    </li>
                    <!-- End Home -->

                    <!-- Pages -->
                    <li >
                        <a href="all_products" >
                            Products
                        </a>

                    </li>
                    <!-- End Pages -->

                    <!-- Blog -->
                    <li>
                        <a href="catalogue">
                            CATALOGUE
                        </a>

                    </li>
                    <!-- End Blog -->

                    <!-- Portfolio -->
                    <li>
                        <a href="factorytour">
                            FACTORY TOUR
                        </a>

                    </li>
                    <!-- End Portfolio -->

                    <!-- Features -->
                    <li>
                        <a href="about">
                            ABOUT
                        </a>

                    </li>
                    <!-- End Features -->

                    <!-- Shortcodes -->
                    <li>
                        <a href="contact">
                            CONTACTS
                        </a>

                    </li>
                    <!-- End Shortcodes -->


                </ul>
            </div><!--/navbar-collapse-->
        </div>
    </div>
    <!-- End Navbar -->
</div>