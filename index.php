<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title>Home</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
   <!-- <link rel="shortcut icon" href="favicon.ico">-->

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-v2.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/parallax-slider/css/parallax-slider.css">
    <link rel="stylesheet" href="assets/plugins/fancybox/source/jquery.fancybox.css">
    <link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">
</head>

<body class="header-fixed">

<div class="wrapper">
    <!--=== Header v2 ===-->
    <?php include('header.php'); ?>
    <!--=== End Header v2 ===-->

    <!-- Image Gradient -->
    <div class="interactive-slider-v2">
    <div class="container">
        <h1>Welcome to MOON MARBLES</h1>
        <p>Best Quality all over the Pakistan.</p>
    </div>
</div>
    <!-- End Image Gradient -->

    <!--=== Content ===-->
    <div class="content-md">
        <div class="container">
            <!-- Service Box -->
            <ul class="list-unstyled row">
                <div class="headline-center margin-bottom-60">
                    <h2>TYPES OF MARBELS WE PROVIDE</h2>
                    </div>


                <li class="col-sm-3 col-xs-6 md-margin-bottom-30">
                    <div class="team-img">
                        <img class="img-responsive" src="assets/img/team/img1-md.jpg" alt="">

                    </div>
                    <h3>BLACK MARBEL</h3>

                    <p>Black is the colour most commonly associated to luxury and elegance. As a sombre colour, it is recommended only for use in well lit rooms and wide spaces, such as bathrooms and kitchens. On the other hand, it can also be used in façades and for cladding without any issues</p>
                </li>
                <li class="col-sm-3 col-xs-6 md-margin-bottom-30">
                    <div class="team-img">
                        <img class="img-responsive" src="assets/img/team/img2-md.jpg" alt="">

                    </div>
                    <h3>RED MARBEL</h3>

                    <p>Red colour adds charm and sensuality to any home, in addition to light and elegance, depending on the tone used.</p>
                </li>
                <li class="col-sm-3 col-xs-6">
                    <div class="team-img">
                        <img class="img-responsive" src="assets/img/team/img3-md.jpg" alt="">

                    </div>
                    <h3>BROWN MARBEL</h3>

                    <p>From a decorative point of view, brown is one of the most versatile colours available, while inspiring safety and comfort. On the other hand, if you are looking to decorate a room with an ethnic look, brown marble is an excellent choice.</p>
                </li>
                <li class="col-sm-3 col-xs-6">
                    <div class="team-img">
                        <img class="img-responsive" src="assets/img/team/img5-md.jpg" alt="">

                    </div>
                    <h3>PINK MARBEL</h3>

                    <p>Vintage fashion brings pink marble into our homes. Pink is the quintessential symbol of calm, delicacy and positivity.</p>
                </li>
            </ul>
            <!-- End Service Box -->
        </div><!--/container -->

        <!-- End Parallax Section -->




        <!-- End Flat Background Block -->

        <div class="container">
            <div class="headline-left margin-bottom-40">
                <h2 class="headline-brd">ONGOING PROJECT</h2>
            </div><!--/end Headline Left-->

            <div class="row margin-bottom-60">
                <div class="col-sm-6">
                    <!-- Owl Carousel v5 -->
                    <div class="owl-carousel-v5">
                        <div class="owl-slider-v5">
                            <div class="item">
                                <img class="full-width img-responsive" src="assets/img/main/img12.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="full-width img-responsive" src="assets/img/main/img16.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="full-width img-responsive" src="assets/img/main/img23.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <!-- End Owl Carousel v5 -->
                </div>
                <div class="col-sm-6">

                    <p>We supply best quality natural stone tiles and slabs for any commercial or residential project. We ship around the world and we make sure that you get high quality, ready to install natural stone, Marble and Granite products when you need it. We are your trusted marble and granite tiles supplier from Pakistan.</p><br>
                   <!-- <button type="button" class="btn-u text-uppercase">View More</button>-->
                </div>
            </div><!--/end row-->
        </div><!--/end container-->

        <!-- Flat Testimonials -->


        <div class="container">
            <div class="headline-center margin-bottom-60">
                <h2>Here are the three little things making us special</h2>

            </div><!--/end Headline Center-->

            <div class="row margin-bottom-40">
                <div class="col-md-4">
                    <div class="content-boxes-v5 margin-bottom-30">
                        <i class="rounded-x icon-rocket"></i>
                        <div class="overflow-h">
                            <h3 class="no-top-space">From Zero to One</h3>
                            <p>We are moving from zero to one, not from one to hundred. That means we are focused on creating new solutions, not copying the existing ones.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="content-boxes-v5 margin-bottom-30">
                        <i class="rounded-x icon-energy "></i>
                        <div class="overflow-h">
                            <h3 class="no-top-space">Our Level Up</h3>
                            <p>Working with us you will work with professional certified designers and engineers having the vast good experience.</p>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="content-boxes-v5 margin-bottom-30">
                        <i class="rounded-x icon-heart"></i>
                        <div class="overflow-h">
                            <h3 class="no-top-space">Your Satisfaction</h3>
                            <p>Creating a design it is not an end in itself, but a means. We understand the rules of the games. It’s about to make you happy.</p>
                        </div>
                    </div>

                </div>
            </div><!--/end row-->
        </div><!--/end container-->
    </div>
    <!--=== End Content ===-->

    <!-- Image Mouse -->
    <!-- End Image Mouse -->

    <!--=== Footer Version 1 ===-->
    <?php include ('footer.php'); ?>
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<!--<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>-->
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<!--<script type="text/javascript" src="assets/plugins/jquery.parallax.js"></script>
<script type="text/javascript" src="assets/plugins/parallax-slider/js/modernizr.js"></script>
<script type="text/javascript" src="assets/plugins/parallax-slider/js/jquery.cslider.js"></script>-->
<script type="text/javascript" src="assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/plugins/fancy-box.js"></script>
<script type="text/javascript" src="assets/js/plugins/owl-carousel.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        App.initParallaxBg();
        FancyBox.initFancybox();
        OwlCarousel.initOwlCarousel();
    });
</script>
<!--[if lt IE 9]>
<!--<script src="assets/plugins/respond.js"></script>
<script src="assets/plugins/html5shiv.js"></script>
<script src="assets/plugins/placeholder-IE-fixes.js"></script>-->
<![endif]-->

</body>
</html>