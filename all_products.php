<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <title>Products</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <!-- <link rel="shortcut icon" href="favicon.ico">-->

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css'
          href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-v2.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/parallax-slider/css/parallax-slider.css">
    <link rel="stylesheet" href="assets/plugins/fancybox/source/jquery.fancybox.css">
    <link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">
    <style>
        .glyphicon {
            margin-right: 5px;
        }

        .thumbnail {
            margin-bottom: 20px;
            height: 420px;
            padding: 0px;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            border-radius: 0px;
            position: relative;
        }

        .item.list-group-item {
            float: none;
            width: 100%;

            background-color: #fff;
            margin-bottom: 10px;
        }

        .item.list-group-item:nth-of-type(odd):hover, .item.list-group-item:hover {
            background: #72c02c;
        }

        .item.list-group-item .list-group-image {
            margin-right: 10px;
        }

        .item.list-group-item .thumbnail {
            margin-bottom: 0px;
        }

        .item.list-group-item .caption {
            padding: 9px 9px 0px 9px;
        }

        .item.list-group-item:nth-of-type(odd) {
            background: #eeeeee;
        }

        .item.list-group-item:before, .item.list-group-item:after {
            display: table;
            content: " ";
        }

        .item.list-group-item img {
            float: left;
        }

        .item.list-group-item:after {
            clear: both;
        }

        .list-group-item-text {
            margin: 0 0 11px;
        }

        .interactive-slider-v2 {
            z-index: 1;
            padding: 200px 0;
            position: relative;
            text-align: center;
            background: url(img/12.jpg) no-repeat;
            background-size: cover;
            background-position: center center;
        }

        .anchor-position {
            position: absolute;
            bottom: 8px;
        }

    </style>

</head>
<body class="header-fixed">

<div class="wrapper">
    <!--=== Header v2 ===-->
    <?php include('header.php'); ?>
    <!--=== End Header v2 ===-->

    <!-- Image Gradient -->
    <div class="interactive-slider-v2">
        <div class="container">
            <h1>PRODUCTS</h1>

        </div>
    </div>

    <!-- list start-->
    <div class="content-md">
        <div class="container">
            <div class="container">
                <div class="headline"><h1 class="text-center"style="margin-bottom: 40px">Products</h1></div>

            </div>

            <div id="products" class="row list-group">
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/1.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Golden Camel Marble</h4>
                            <p class="group inner list-group-item-text">
                                Golden camel marble the perfect piece that always fits in. Classic and graceful, golden
                                camel marble tiles and slabs carry mellowness and coziness in equal parts and all in a
                                very natural and melodious manner.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=golden_camel">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/2.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Lakrol Fancy Marble</h4>
                            <p class="group inner list-group-item-text">
                                Lakrol Fancy Marble is the wavy type of marble which can be used in bathroom, kitchen
                                and flooring. It looks so attractive which is the best thing in this natural marble
                                stone..</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Lakrol Fancy">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/3.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Royal Fancy Marble</h4>
                            <p class="group inner list-group-item-text">
                                Royal Fancy Marble is well known for its beautiful texture and its decent color. This
                                fancy marble has various textures and shades like mildish light fancy, classy dark
                                fancy, aggressive desert storm fancy cross and cut royal fancy marble. </p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u margin-top-20" href="product.php?var=Royal Fancy">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/4.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Pakistani Cicilia Marble</h4>
                            <p class="group inner list-group-item-text">
                                Pakistani Cicilia marble is well known because of its beautiful design and elegant
                                texture. We as a reputed firm and our company considered as a leading company that
                                manufacture, supply and trade high quality and a large range of Pakistani Cicilia
                                marble.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Pakistani Cicilia">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/5.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Fossil Brown Marble</h4>
                            <p class="group inner list-group-item-text">
                                Fossil brown marble has its own attractive and pleasing texture. Fossil brown is also
                                known as Bruno Etrusco, Brown Fossil, Fossil Marrone, Marron Etrusco, Brown Fossil
                                Limestone, Fossil Marmor braun, Jurassic sea marble, Cānghǎi zhū luó (in China stone
                                market)</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Fossil Brown">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/6.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Sahara Beige Marble</h4>
                            <p class="group inner list-group-item-text">
                                Sahara Beige Marble is one of the most popular varieties of Pakistan marble. This marble
                                comes in Pink and beige, chocolate, red, brown green and gray shades. Low water
                                absorption, high hardness, resistance to heat and weather corrosion, uniform color and
                                luster make it a popular variety.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Sahara Beige">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/7.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Nova Beige Marble</h4>
                            <p class="group inner list-group-item-text">
                                As a kind of beige marble from Pakistan, Nova Beige Marble is commonly used in Building
                                stone, fireplaces, stairs, countertops, sinks, monuments, pool coping, pavers, sills,
                                ornamental stone, mosaic, fountain, sculpture and so on.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Nova Beige">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/8.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Bubble Limestone</h4>
                            <p class="group inner list-group-item-text">
                                Bubble Limestone is a natural stone, just like marble, granite, and limestone—in fact,
                                bubble marble is a type of limestone, which can be use vanity top, Wall tile, window
                                sills, floor tile, project tile, stair, etc counter top, fountain, column, baluster,
                                handrail,etc statue, curb stone, etc.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Bubble Limestone">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/9.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Flower Beige Marble</h4>
                            <p class="group inner list-group-item-text">
                                Flower Beige Marble is a kind of beige marble quarried in Turkey. This stone is
                                especially good for Exterior – Interior wall and floor applications, monuments,
                                countertops, mosaic, fountains, pool and wall capping, stairs, window sills, etc and
                                other design projects.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Flower Beige">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/10.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Travertine Marble</h4>
                            <p class="group inner list-group-item-text">
                                Travertine marble is a natural stone, just like marble, granite, and limestone—in fact,
                                travertine is a type of limestone. However, they are not the same. Travertine has some
                                telltale characteristics that separate it from regular limestone.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Travertine Marble">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/11.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Butak Onyx</h4>
                            <p class="group inner list-group-item-text">
                                The working of the Butak Green Onyx tiles requires a special attention during the
                                selection phase, that is the quality check of the materials. The tiles obtained from the
                                best blocks, are finally checked and selected one by one, so as to be able to assure the
                                final Customers a high quality and excellent selection.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Butak Onyx">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/12.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Yellow Onyx</h4>
                            <p class="group inner list-group-item-text">
                                Pakistan yellow onyx is popular yellow onyx from Pakistan onyx quarry, Pakistani onyx is
                                famous in the world. Yellow onyx products include golden onyx tile for interior wall,
                                golden onyx slabs, golden onyx countertop, gold onyx vanity tops and tub surrounds,
                                golden onyx table top, gold onyx sink and basin, carvings, tiles and Slabs etc.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Yellow Onyx">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/13.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Coffee Brown Marble</h4>
                            <p class="group inner list-group-item-text">
                                After understanding the needs and expectations of the growing industry, we are
                                presenting the Coffee Brown Marble. We manufacture and export the marbles in different
                                shades and sizes.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Coffee Brown">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/14.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                White Limestone</h4>
                            <p class="group inner list-group-item-text">
                                White Limestone is especially good for Exterior – Interior wall and floor applications,
                                mosaic, fountains, monuments, counter tops pool, stairs, window sills and wall capping
                                etc.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=White Limestone">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/15.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Black and Gold Marble</h4>
                            <p class="group inner list-group-item-text">
                                Black and gold marble (Michelangelo) is a very unique and famous Pakistani marble, it is
                                famous because of its black color on which there are white & golden stripes. It is also
                                called and knows as Black & Gold. </p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Black and Gold">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/16.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Light Green Onyx</h4>
                            <p class="group inner list-group-item-text">
                                Pakistani Light Green Onyx is really attractive due to its light shades as well as
                                stunning and a modern look. Because of its specific design, it offers striking and
                                dramatic appeal, it is most demanding worldwide. Pakistan is considered a very lucky,
                                wealthy and blessed because it has a huge amount of world’s best quality Onyx
                                resources.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Light Green">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/17.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Afghan Green Onyx</h4>
                            <p class="group inner list-group-item-text">
                                Afghan Green Onyx Slabs for export sold per square foot or meter. These slabs measure:
                                Length: 4 to 6 feet OR 48 to 72 inches OR 120 to 180 cm Width: 3 to 5 feet OR 36 to 60
                                inches OR 90 to 150 cm and Onyx Slabs Thickness: 2 and 3 cm OR 20 and 30 mm, based on
                                the characteristics of the materials. </p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Afghan Green">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/18.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Multi Red Onyx</h4>
                            <p class="group inner list-group-item-text">
                                A great alternative to granite, marble, and other more popular stone surfaces, onyx
                                provides a sleek and modern look to any workspace. Onyx is formed in caves as
                                stalactites and stalagmites drip to form this precious stone. Onyx Countertops are a
                                rarity as they bring a unique appearance with a strikingly dramatic appeal.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Multi Red">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/19.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Multi Green Onyx</h4>
                            <p class="group inner list-group-item-text">
                                Multi Green Onyx is a beautiful cryptocrystalline form of natural stone but it’s calcite
                                like marble, it is denser, translucent and more brittle. It is extracted from Pakistan’s
                                natural reserves existing in Baluchistan province. Pakistan has specific variations of
                                Onyx, most common favorite is green among others.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Multi Green">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/20.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                White Onyx</h4>
                            <p class="group inner list-group-item-text">
                                Standing out of the plenty of Competition demands accuracy, reliability as well as
                                timely production with exact specifications and delivery according to promises. Our fair
                                dealings with our respected clients made us capable in white onyx importer’s eye to use
                                our supplied material for some of the prestigious building projects in the Gulf
                                region.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u " href="product.php?var=White Onyx">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/21.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Indus Gold Marble</h4>
                            <p class="group inner list-group-item-text">
                                Indus Gold marble are available in varied golden colors, sizes and at affordable cheap
                                price. It adds a timeless charm to the modern day building. With its lush dark golden
                                color, it radiates a pleasing and soothing appeal.<br><br></p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Indus Gold">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/22.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Dark Green Onyx</h4>
                            <p class="group inner list-group-item-text">
                                Usually, every landlord has the tremendous desire of getting commercial and residential buildings constructed in an artistic and wonderful way and that makes them satisfied. Pakistan is blessed by an abundance of natural minerals reserves including resources of green onyx. Light transmitting ability of Onyx really produces a striking effect.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Dark Green">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item  col-xs-4 col-lg-4">
                    <div class="thumbnail">
                        <img class="group list-group-image" src="img/list/23.jpg" alt=""/>
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                Yellow Sandstone</h4>
                            <p class="group inner list-group-item-text">
                                Yellow Sandstone is a beautiful natural Sandstone exhibiting a mixture of yellow, buff, and golden colors. With its natural tones and shades, Yellow Sandstone paving blends in with almost any surrounding. Yellow is especially useful in exterior claddings in sea shore buildings due to acid and thermal resistant properties. As such the effect of saline winds is negligible on Yellow Sandstone.</p>
                            <div class="row">

                                <div class="col-xs-12 col-md-6 anchor-position">
                                    <a class="btn btn-u" href="product.php?var=Yellow Sandstone">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
    <!--list end -->
    <!--=== Footer Version 1 ===-->
    <?php include ('footer.php'); ?>
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<!--<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>-->
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<!--<script type="text/javascript" src="assets/plugins/jquery.parallax.js"></script>
<script type="text/javascript" src="assets/plugins/parallax-slider/js/modernizr.js"></script>
<script type="text/javascript" src="assets/plugins/parallax-slider/js/jquery.cslider.js"></script>-->
<script type="text/javascript" src="assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/plugins/fancy-box.js"></script>
<script type="text/javascript" src="assets/js/plugins/owl-carousel.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        App.init();
        App.initParallaxBg();
        FancyBox.initFancybox();
        OwlCarousel.initOwlCarousel();
    });
    $(document).ready(function () {
        $('#list').click(function (event) {
            event.preventDefault();
            $('#products .item').addClass('list-group-item');
        });
        $('#grid').click(function (event) {
            event.preventDefault();
            $('#products .item').removeClass('list-group-item');
            $('#products .item').addClass('grid-group-item');
        });
    });
</script>
<!--[if lt IE 9]>
<!--<script src="assets/plugins/respond.js"></script>
<script src="assets/plugins/html5shiv.js"></script>
<script src="assets/plugins/placeholder-IE-fixes.js"></script>-->
<![endif]-->

</body>
</html>